# Renko Analysis

## Introduction

![Renko Chart](./sample.png)

This script allows you to take tick-level data and convert and plot it into a
renko-based time-independent plots and csv.

## Installation

- Install the dependencies from the `Pipfile`.

```
pipenv install --skip-lock
```

## Running

```
usage: renko.py [-h] [-i INPUT] [-b BRICK_SIZE] [-p PLOT] [-o OUTPUT]
                [--pip PIP]

Parse OHLC into renko

optional arguments:
  -h, --help            show this help message and exit
  -i INPUT, --input INPUT
                        Tick level CSV to convert to renkos
  -b BRICK_SIZE, --brick-size BRICK_SIZE
                        Block size in pips
  -p PLOT, --plot PLOT  Path to save dynamic plot
  -o OUTPUT, --output OUTPUT
                        Output path for renko-CSV
  --pip PIP             Default pipsize for the instrument
```
