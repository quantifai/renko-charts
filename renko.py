#!/usr/bin/env python3
# coding: utf-8
"""
    :author: Pratik K
    :brief: Calculate renko chart given tick data
"""
import argparse
from enum import Enum
import os
import pickle

import matplotlib
from matplotlib.collections import PatchCollection
from matplotlib.patches import Rectangle
import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator, MultipleLocator
from mpl_finance import candlestick2_ohlc
import pandas as pd
import talib as ta


# global declarations
args = None
class Bar(Enum):
    WHITE = 0
    BLACK = 1


def collect_args() -> argparse.Namespace:
    """CLI args to collect and parse"""
    parser = argparse.ArgumentParser(description="Parse OHLC into renko")
    parser.add_argument('-i', '--input', type=str, default='ticks.csv',
                        help="Tick level CSV to convert to renkos")
    parser.add_argument('-b', '--brick-size', type=float, default=2,
                        help="Block size in pips")
    parser.add_argument('-p', '--plot', type=str, default='renko.fig.pkl',
                        help="Path to save dynamic plot")
    parser.add_argument('-o', '--output', type=str, default='renko.csv',
                        help="Output path for renko-CSV")
    parser.add_argument('--pip', type=float, default=0.0001,
                        help="Default pipsize for the instrument")
    args = parser.parse_args()
    return args


def calc_renkos(ticks: pd.DataFrame, brick_size: float) -> pd.DataFrame:
    """Given ticks, calculate renko bars into a dataFrame

    :param ticks: pd.DataFrame -> tick df
    :param brick_size: float -> size in pips of each brick
    """
    mids = ((ticks.ask + ticks.bid) / 2.0).reset_index(drop=True)
    len_ = mids.shape[0]
    renkos, cur_renko = [], []
    prev_close = mids[0]
    prev_bar = None
    start = mids[0]
    cur_val = start
    up_target = start + brick_size * args.pip
    down_target = start - brick_size * args.pip
    for t_i in range(len_):
        cur_val = mids.iat[t_i]
        cur_renko.append(cur_val)

        if cur_val >= up_target or cur_val <= down_target:
            prev_bar = Bar.BLACK if cur_renko[-1] > cur_renko[0] else Bar.WHITE
            renkos.append(cur_renko)
            start = cur_val
            if prev_bar == Bar.BLACK:  # cur BLACK
                up_target = start + brick_size * args.pip
                down_target = start - 2 * brick_size * args.pip
            else:  # cur WHITE
                up_target = start + 2 * brick_size * args.pip
                down_target = start - brick_size * args.pip
            cur_renko = [start]
    renko_df = pd.DataFrame([[_[0], max(_), min(_), _[-1]] for _ in renkos],
                            columns=['open', 'high', 'low', 'close'])
    del renkos
    return renko_df


def plot_renko(df: pd.DataFrame) -> matplotlib.figure.Figure:
    """Plot renko dataFrame"""
    fig, ax = plt.subplots(1)
    ax.set_xlim([-1.0, df.shape[0] + 1.0])
    ax.set_ylim([df.low.min() - 3 * args.pip, df.high.max() + 3 * args.pip])
    ax.xaxis.set_major_locator(MultipleLocator(20))
    ax.yaxis.set_major_locator(MultipleLocator(5 * args.brick_size * args.pip))

    ax.xaxis.set_minor_locator(AutoMinorLocator(5))
    ax.yaxis.set_minor_locator(AutoMinorLocator(args.brick_size * args.pip))

    ax.grid(which='major', color='0.4', alpha=0.5)
    ax.grid(which='minor', color='0.5', linestyle=':', alpha=0.4)

    candlestick2_ohlc(ax, df.open, df.high, df.low, df.close, colorup='0.1',
                      colordown='0.6', width=0.75, alpha=0.9)
    ax.autoscale_view()
    return fig


def main():
    global args
    args = collect_args()
    ticks = pd.read_csv(args.input, parse_dates=[0], index_col=0)
    print("Calculating renkos ...")
    renkos = calc_renkos(ticks, args.brick_size)
    renkos.to_csv(args.output, index=False)

    print("Plotting renkos ...")
    fig = plot_renko(renkos)
    plt.title(f"Renko [{args.brick_size} pips] (pipsize: {args.pip})")
    pickle.dump(fig, open(args.plot, 'wb'))
    plt.tight_layout()
    plt.show()


if __name__ == "__main__":
    main()

